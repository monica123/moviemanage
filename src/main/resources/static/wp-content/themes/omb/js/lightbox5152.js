/*!
 * OMB Lightbox Plugin
 */
;(function ( $, window, document, undefined ) {

    $.fn.ombLightbox = function ( options ) {
        options = $.extend( {}, $.fn.ombLightbox.options, options );

        return this.each(function () {

            var elm 						= $(this),
                data               			= elm.data('lightbox'),
                lightbox_elm        		= $('<div id="omb-lightbox-overlay"></div>'),
                lightbox_mask       		= $('<span id="omb-lightbox-mask"></span>'),
                lightbox_content_wrapper    = $('<div id="omb-lightbox-content-wrapper" />'),
                lightbox_content			= $('<div id="omb-lightbox-content" />'),
                lightbox_header 			= $('<div class="omb-lightbox-header" />');
            
            function init()
            {
            	triggers();
            }
			
            function trailer_content()
            {
              var html = [];

              if( data.video.type == 'youtube' ){
                html.push( '<iframe width="100%" height="100%" src="//youtube.com/embed/' + ( data.video.id ) + '" frameborder="0" allowfullscreen></iframe>' );
              }
              else if( data.video.type == 'vimeo' ){
                html.push( '<iframe width="100%" height="100%" src="//player.vimeo.com/video/' + ( data.video.id ) + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' );
              }

              lightbox_content.html( html.join("\n") );
            }
            
            function ajax_content()
            {
            	$.ajax(js_vars.ajaxurl, {
            		'dataType': 'html',
	            	'method': 'POST',
            		data: {
	            		'action': data.action,
	            		'sub_action': data.sub_action,
	            		'params': data.params
	            	}
            	}).done(function( response ) {
            		lightbox_content.html( response );
            		
            		options.onEnd();
            	});
            }
            
            function embed_content()
            {
              if( typeof(data.type) != "undefined" ){
                // case 1, trailer lightbox content
                if( data.type == 'trailer' ){
                  trailer_content();
                }
                
                // case 2, ajax
                if( data.type == 'ajax' ){
                  ajax_content();
                }
              }
            }

            function create_lightbox()
            {
                // remove old lightbox
                $("#omb-lightbox-overlay").remove();
                lightbox_content.html('');
				
                lightbox_elm.html( lightbox_mask );
        				lightbox_elm.append( lightbox_content_wrapper );
        				
        				if( options.header.show_header == true ){
        					
        					lightbox_header.html('');
        					
        					if( options.header.title != '' ) {
        						lightbox_header.append( '<h4 class="title">' + options.header.title + '</h4>' );
        					}
        					
        					if( options.header.close_btn != '' ) {
        						lightbox_header.append( '<a href="#" id="omb-lightbox-close"><i class="fa fa-times"></i></a>' );
        					}
        					
        					if( lightbox_header.html() != '' ) {
        						lightbox_content_wrapper.append( lightbox_header );
        					}
        				}
        				
        				// start preloader
        				ombPreloader( lightbox_content );       
				
                embed_content();
				
                lightbox_content_wrapper.append( lightbox_content );

                $('body').append(lightbox_elm);
                
                style_lightbox();

                lightbox_open();
            }

      			function style_lightbox()
      			{
      				lightbox_elm.css({
      					'background-color': options.overlay_color
      				});
      				
      				lightbox_content_wrapper.css({
      					'padding': options.padding,
      					'width': options.content_size.width,
      					'height': options.content_size.height
      				});
      				
      				var content_height = lightbox_content_wrapper.height();
      				if( options.header.show_header == true && parseInt(options.header.height) > 0 ){
      					content_height = content_height - options.header.height;
      				}
      				
              if( data.type == 'trailer' ) {
                lightbox_content.css({ 'height': content_height + "px", 'overflow': 'inherit' });
              }

      				//lightbox_content.css( 'height', content_height + "px" );
      				
      				// top center
      				//lightbox_content_wrapper.css( 'margin-left', "-" + (lightbox_content_wrapper.width() / 2) + "px" );
      				//lightbox_content_wrapper.css( 'margin-top', "-" + (lightbox_content_wrapper.height() / 2) + "px" );

      			}

            function lightbox_open()
            {
               lightbox_elm.fadeIn( options.open_speed );
            }

            function lightbox_close()
            {
              lightbox_elm.fadeOut( options.close_speed, function(){
                lightbox_elm.remove();
              });
            }

            function triggers()
           	{
           		elm.click(function(e){
                if( typeof elm.attr('href') == 'undefined' || elm.attr('href').indexOf('#') > -1 ) {
                    e.preventDefault();
                    create_lightbox();
                }
              });
				
      				$(document).on('click', '#omb-lightbox-mask, #omb-lightbox-close, .omb-lightbox-close', function(e){
      					e.preventDefault();
      					lightbox_close();
      				});
           	}

            init();
        });
    };

    $.fn.ombLightbox.options = {
    	'header': {},
        'overlay_color' : 'rgba(0,0,0, 0.4)',
        'padding'		: '0px',
        'content_size' : {
          'width': '300px',
          'height': '100%'
        },
        'open_speed': 250,
        'close_speed': 250,
        onStart: function ( elem, param ) {},
        onEnd: function ( elem, param ) {}
    };

})( jQuery, window, document );