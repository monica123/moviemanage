;(function ( $, window, document, undefined ) {
	"use strict";

    $.fn.ombBookNow = function ( options ) {
        options = $.extend( {}, $.fn.ombBookNow.options, options );

        var wrapp_selectors = this,
			selectors 		= wrapp_selectors.find("select"),
			alias_val 		= [];
			
        return this.each(function () {
			
			function reverse_arr(input) 
			{
			    var ret = [];
			    for(var i = input.length-1; i >= 0; i--) {
			        ret.push(input[i]);
			    }
			    return ret;
			}

			function disable_next( curr )
			{
				if( curr.size() > 0 ){
					curr.find( "option:first-child").attr("selected", "selected");
					curr.removeAttr('disabled');

					var rel = $( "select#" + curr.data('rel') );
					
					if( rel.size() > 0 ){
						disable_next( rel );
					}
				}
			}
			
			function get_prev_value( curr )
			{
				if( curr.size() > 0 ){
					
					alias_val.push( curr.val().replace(" ", "-") );
					var prev_elm = curr.parent('.custom-dropdown-wrapper').prev('.custom-dropdown-wrapper').find('select');
					if( prev_elm.size() > 0 ){
						get_prev_value( prev_elm );
					}
				}
			}
			
			function print_next_btn( select )
			{
				var alias = [];
				selectors.each(function(){
					var that = $(this),
						val = that.val().replace(" ", "-");
					
					alias.push( val );
				});
				
				alias = alias.join("#");
				var __ = wrapp_selectors.data('alias');
				
				if( typeof(__[alias]) != "undefined" ){
					
					if( wrapp_selectors.attr('id') == 'omb-quick-booking-filters' ) {
						
						$('.btn_schedule_next').data('lightbox', '{"type":"ajax","action":"omb_pt_event_ajax_requests","sub_action":"get_hall","params":{"show_movie":true,"event_id":'+ __[alias] +'}}');
						
						// DO NOT open lightbox if tablet/mobile
						if( $(window).width() > 992 ) {
							$(".btn_schedule_next").ombLightbox({
								'header': {
									'show_header': true,
									'close_btn': true,
									'title': 'Reserve your ticket',
									'height': 80
								},
								'overlay_color' : 'rgba(0,0,0, 0.4)',
						        'content_size' : {
						          'width': '1200px',
						          'height': '850px'
						        }
							});
						}
					}

					$('.btn_schedule_next').data( "event", __[alias] ).css('display','inline-block');
				}else{
					$('.btn_schedule_next').hide();
				}
			}
			
			function triggers()
			{
				selectors.each(function(){
					var that = $(this);
					
					that.on('change', function(){
						alias_val = [];
						var that2 = $(this),
							val = that2.val().replace(" ", "-"),
							rel = $( "select#" + that2.data('rel') );
							
						if( rel.size() > 0 ){
							rel.find( "option:not(:first-child)" ).hide();
							
							get_prev_value( that2 );
							
							alias_val = reverse_arr( alias_val );
							 
							val = alias_val.join("-"); 
		 
							rel.find( "option.omb-" + val ).show();
							
							// select first for next selectors
							disable_next( rel );
						}
						
						print_next_btn( that );
					});
				});
			
				disable_next( selectors.eq(0) );
			}

            triggers();
        });
    };

	$.fn.ombBookNow.options = {
        onStart: function ( elem, param ) {},
        onEnd: function ( elem, param ) {}
    };

})( jQuery, window, document );