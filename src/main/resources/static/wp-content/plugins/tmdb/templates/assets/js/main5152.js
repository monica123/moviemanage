function tmdbPreloader(elm, fullscreen)
{
	var html = [];
    html.push('<div class="tmdb-preload '+ (fullscreen == true ? 'tmdb-preload-fullscreen' : '') +'">');
    html.push(    '<div class="tmdb-preload-status">');
    html.push(        '<div class="tmdb-sk-spinner tmdb-sk-spinner-double-bounce">');
    html.push(            '<div class="tmdb-sk-double-bounce1"></div>');
    html.push(            '<div class="tmdb-sk-double-bounce2"></div>');
    html.push(        '</div>');
    html.push(    '</div>');
    html.push('</div>');

    elm.append(html.join("\n"));
}

function tmdbPreloader_hide( elm ) {
	jQuery(elm).find('.tmdb-preload').fadeOut(function() {
		jQuery(this).remove();
	});
}

;(function($) { "use strict";

	function replaceUrlParam(url, paramName, paramValue)
	{
	    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
	    if(url.search(pattern)>=0){
	        return url.replace(pattern,'$1' + paramValue + '$2');
	    }
	    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue 
	}

	function build_movie_filters()
	{
		$(".tmdb-movie-filters").each(function(){
			var that = $(this),
				page_url = window.location.href,
				order = that.data('order'),
				order_dir = that.data('orderdir');

			that.find("a#tmdb-movie-filter-" + order ).addClass("on").addClass("tmdb-order-dir-" + order_dir);

			that.find("a").each(function(){
				var that2 = $(this),
					new_order = that2.attr('id').replace('tmdb-movie-filter-', ''),
					new_url = replaceUrlParam(page_url, 'order', that2.attr('id').replace('tmdb-movie-filter-', '') );

				if( new_order == order ){
					new_url = replaceUrlParam(new_url, 'order_dir', ( order_dir == 'ASC' ? 'desc' : 'asc' ) )
				}

				that2.attr('href', new_url);
			});
		});
	}

	$(document).ready(function(){
		build_movie_filters();
		
		$("#movie-slideshow").tmdbSlideshow({
			'show_thumbs': true,
			'number_thumbs': 5
		});

		$('body').on('click', "a.tmdb-show-all-btn", function(e){
			e.preventDefault();
	
			var that = $(this),
				rel_elm = $(that.data('rel')),
				hiddenclass = that.data("hiddenclass");
	
			rel_elm.find(hiddenclass).removeClass( hiddenclass.replace( ".", "" ) );
			that.remove();
		});

		$(".tmdb-lightbox").tmdbLightbox({
			'header': {
				'show_header': true,
				'close_btn': true,
				'title': 'Trailer',
				'height': 80
			},
			'overlay_color' : 'rgba(0,0,0, 0.4)',
			'close_btn': false,
	        'content_size' : {
	          'width': '640px',
	          'height': '480px'
	        },
		});
	});
})(jQuery);