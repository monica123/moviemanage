package com.movie.booking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String movieName;
    private String leadRole;
    private String writer;
    private String director;
    private String producer;
    private String production;
    private Date releaseDate;
    private String language;

    @Transient
    private MultipartFile movieImage;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    private boolean bookingOpen = true;


}
