package com.movie.booking.controller.AdminPanel;

import com.movie.booking.model.User;
import com.movie.booking.repository.RoleRepository;
import com.movie.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;
    @GetMapping(value="/admin/dashboard")
    public String getAdminDashboard(Model model, Principal principal){
        User user =new User();
        if(principal != null) {
            String username = principal.getName();
            user = userService.findByUsername(username);

            model.addAttribute("user", user);
        }
       String rolename=roleRepository.getRoleName(user.getUsername());

            return "adminpanel";

    }
    @GetMapping(value = "/contactus")
    public String getMovieContact(){
        return "contactus";
    }

}
