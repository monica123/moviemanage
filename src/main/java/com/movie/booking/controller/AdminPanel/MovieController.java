package com.movie.booking.controller.AdminPanel;

import com.google.gson.Gson;
import com.movie.booking.model.Genre;
import com.movie.booking.model.Movie;

import com.movie.booking.model.User;

import com.movie.booking.repository.GenreRepository;


import com.movie.booking.service.MovieService;
import com.movie.booking.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.util.List;

@Controller
public class MovieController {
    @Autowired
    private MovieService movieService;

    @Autowired
    private UserService userService;

    @Autowired
    private GenreRepository genreRepository;

    @GetMapping(value = "/add/movie")
    public String getMovie(Model model){
        List<Genre> genreList = genreRepository.findAll();
        Gson gson=new Gson();
        System.out.println("asdasdsd"+gson.toJson(genreList));
       model.addAttribute("genrelist", genreList);
        System.out.println(new Gson().toJson(genreList));
        return "admintemplates/moviesection/addmovie";
    }

    @PostMapping(value="/add/movie")
    public String  saveHall(@ModelAttribute Movie movie, HttpServletRequest request, RedirectAttributes redirectAttributes){

        System.out.println(movie.toString());

       Long movieId= movieService.save(movie);
        System.out.println("movie id is"+movieId);

//       for(int genreId:movieGenre){
//        movieGenreRepository.saveMovieGenre(Math.toIntExact(movieId),genreId);
//       }

        MultipartFile movieImage = movie.getMovieImage();

        try {
            byte[] bytes = movieImage.getBytes();
            String name = movie.getId() + ".png";
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(new File("src/main/resources/static/images/movie/" + name)));
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        redirectAttributes.addFlashAttribute("success","Data Successfully Saved");

        return "redirect:/add/movie";
    }

    @GetMapping(value = "/view/movies")
    public String viewMovieHalls(Model model) {
        model.addAttribute("movielist", movieService.findAll());

        return "admintemplates/moviesection/viewmovies";
    }

    @PostMapping(value = "/edit/movie")
    public String editMovie(@ModelAttribute Movie movie) {
        System.out.println("movie" + movie);
        Movie movie1 = movieService.findOne(movie.getId());
        movie1.setDirector(movie.getDirector());
        movie1.setLanguage(movie.getLanguage());
        movie1.setProducer(movie.getProducer());
        movie1 = movie;
        System.out.println("movie" + movie);
        movieService.save(movie1);
        return "redirect:/view/movies";
    }

    @GetMapping(value = "/delete/movie/{id}")
    public String delMovie(@PathVariable("id") long id) {
        System.out.println("id is" + id);
        try {
            movieService.delete(id);
        } catch (Exception e) {
            System.out.println("Error :" + e.getMessage());
        }

        return "redirect:/view/movies";
    }

    @RequestMapping("/movieshelf")
    public String bookshelf(Model model, Principal principal) {
        if (principal != null) {
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        List<Movie> movieList = movieService.findAll();
        model.addAttribute("movieList", movieList);
        model.addAttribute("activeAll", true);

        return "movie/movieShelf";
    }

    @GetMapping("/movieDetail")
    public String movieDetail(
            @PathParam("id") Long id, Model model, Principal principal
    ) {
        if (principal != null) {
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        Movie movie = movieService.findOne(id);

        model.addAttribute("movie", movie);

        return "movie/movieDetail";
    }

    @GetMapping(value = "/movies-list")
    public String viewMovieForUSer(Model model) {
        model.addAttribute("movielist", movieService.findAll());
        model.addAttribute("genrelist",genreRepository.findAll());
        return "movie/movielist";
    }

    @GetMapping(value = "/movies-list-{id}")
    public String viewMovieByGenre(Model model,@PathVariable("id") long id) {
        model.addAttribute("movielist", movieService.findByGenreType(id));
        model.addAttribute("genrelist",genreRepository.findAll());
        return "movie/movielist";
    }

    @GetMapping(value = "/movieslist")
    public String viewMovieByTitle(Model model,@RequestParam("title") String movieName) {
        System.out.println("herer");
        model.addAttribute("movielist", movieService.findByMovieName(movieName));
        model.addAttribute("genrelist",genreRepository.findAll());
        return "movie/movielist";
    }

    @GetMapping(value = "/add/shows")
    public String addShows(Model model) {
        model.addAttribute("movielist", movieService.findAll());
        return "admintemplates/moviesection/addshows";
    }
}
