package com.movie.booking.repository;

import com.movie.booking.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie,Long> {
    @Query(value = "select * from movie where genre_id=?1",nativeQuery = true)
    List<Movie> findByGenreType(long id);

    @Query(value = "select * from movie where movie_name like  CONCAT('%',?1, '%')",nativeQuery = true)
    List<Movie> findByMovieName(String movieName);

    @Query(value = "select distinct m.* from movie m, movie_details md where  m.id=md.movie_id",nativeQuery = true)
    List<Movie> findMovieToShows();

 }
