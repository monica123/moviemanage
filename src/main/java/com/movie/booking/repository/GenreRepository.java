package com.movie.booking.repository;

import com.movie.booking.model.Genre;


import org.springframework.data.jpa.repository.JpaRepository;


public interface GenreRepository extends JpaRepository<Genre,Long> {



}
