package com.movie.booking.service;

import com.movie.booking.model.User;
import com.movie.booking.model.security.PasswordResetToken;
import com.movie.booking.model.security.UserRole;

import java.util.Set;

public interface UserService {
    PasswordResetToken getPasswordResetToken(final String token);

    void createPasswordResetTokenForUser(final User user, final String token);

    User findByUsername(String username);

    User findByEmail(String email);

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save(User user);

    User findById(Long id);


}
